#!/bin/bash

# Ubuntu, Red Hat, CentOS, Fedora
echo "Port 2541" >> /etc/ssh/sshd_config
apt-get update
apt install fail2ban -y
wget -O /etc/fail2ban/jail.local https://gitlab.com/ezhost.gg/Hosting/-/raw/main/Scripts/Install/Fail2Ban/jail.local
wget -O /etc/fail2ban/action.d/iptables-common.local https://gitlab.com/ezhost.gg/Hosting/-/raw/main/Scripts/Install/Fail2Ban/iptables-common.local
chmod 744 /etc/fail2ban/jail.local 
chmod 744 /etc/fail2ban/action.d/iptables-common.local
apt-get upgrade -y
systemctl restart sshd
systemctl restart fail2ban
reboot
exit
