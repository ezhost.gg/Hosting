#!/bin/sh

openssl x509 -checkend 2592000 -noout -in /etc/ssl/certs/$1 2>&1 1>/dev/null

if [ $? -ne 0 ]; then
  echo "1:$?:$1 Down"
else
  echo "0:$?:OK"
fi
